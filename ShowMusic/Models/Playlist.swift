//
//  Playlist.swift
//  ShowMusic
//
//  Created by Javier Servate on 10/04/2019.
//  Copyright © 2019 ShowPad, NV. All rights reserved.
//

import UIKit

struct Playlist: Codable {
    let title: String
    let albums: [Int]
}
