//
//  PlaylistsCollectionViewController.swift
//  ShowMusic
//
//  Created by Javier Servate on 10/04/2019.
//  Copyright © 2019 ShowPad, NV. All rights reserved.
//

import UIKit
import ShowMusicServices

final class PlaylistsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private var music = Music(playlists: [], albums: [])
    private var data: Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        configureTableView()
        downloadInformation()
    }
    
    private func configureNavigationBar() {
        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.navigationBar.topItem?.title = Constants.Labels.libraryNavigationBar
        navigationController?.navigationBar.barStyle = .black
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func configureTableView() {
        tableView.register(UINib(nibName: Constants.NibNames.playListTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifiers.playlistTableView)
    }
    
    private func downloadInformation() {
        MusicLibrary.shared.downloadMusicLibraryDefinition { [weak self] (result) in
            switch result {
            case .success(let data): self?.setPlaylists(data: data)
            default: return
            }
        }
    }
    
    private func setPlaylists(data: Data) {
        guard let musicTmp = try? JSONDecoder().decode(Music.self, from: data) else { return }
        music = musicTmp
        DispatchQueue.main.async {
            self.tableView?.reloadData()
        }
    }
}

extension PlaylistsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return music.playlists.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return music.playlists[section].title
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.playlistTableView, for: indexPath) as? PlaylistTableViewCell else { return UITableViewCell() }
        
        cell.setContent(playlist: music.playlists[indexPath.section], albums: music.albums)
        return cell
    }
}
