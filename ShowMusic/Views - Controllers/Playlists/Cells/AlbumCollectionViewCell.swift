//
//  AlbumCollectionViewCell.swift
//  ShowMusic
//
//  Created by Javier Servate on 11/04/2019.
//  Copyright © 2019 ShowPad, NV. All rights reserved.
//

import UIKit
import ShowMusicServices

final class AlbumCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setStyle()
    }
    
    private func setStyle() {
        layer.cornerRadius = 6
        layer.masksToBounds = true
        imageView.layer.cornerRadius = 6
        imageView.layer.masksToBounds = true
    }
    
    func setContent(album: Album) {
        titleLabel.text = album.title
        
        let key = NSString(string: String(album.id))
        guard let coverImage = CacheManager.shared.retrieveImage(for: key) else {
            MusicLibrary.shared.downloadAlbumCover(identifier: album.id) { [weak self] (result) in
                switch result {
                case .success(let data):
                    //All the code related to the UI must be done in the main thread
                    DispatchQueue.main.async { [weak self] in
                        guard let coverImage = UIImage(data: data) else { return }
                        self?.imageView.image = coverImage
                        CacheManager.shared.saveImage(image: coverImage, for: key)
                    }
                case .failure(let error):
                    debugPrint(error.localizedDescription)
                    DispatchQueue.main.async { [weak self] in
                        self?.imageView.image = UIImage(named: Constants.placeholderCover)
                    }
                }
            }
            return
        }
        imageView.image = coverImage
    }

}
