//
//  PlaylistTableViewCell.swift
//  ShowMusic
//
//  Created by Javier Servate on 10/04/2019.
//  Copyright © 2019 ShowPad, NV. All rights reserved.
//

import UIKit
import ShowMusicServices

final class PlaylistTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    private var playlist: Playlist?
    private var albums: [Album]?
    
    override func awakeFromNib() {
        configureCollectionView()
    }
    
    func setContent(playlist: Playlist, albums: [Album]) {
        self.playlist = playlist
        self.albums = albums
        collectionView.reloadData()
    }
    
    private func configureCollectionView() {
        collectionView.register(UINib(nibName: "AlbumCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: Constants.CellIdentifiers.albumCollectionView)
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
    }
}

extension PlaylistTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playlist?.albums.count ?? 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.albumCollectionView, for: indexPath) as? AlbumCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.imageView.image = nil
        if let playlist = playlist, let albums = albums {
            cell.setContent(album: albums[playlist.albums[indexPath.row]])
        }
        
        return cell
    }
    
    
}
