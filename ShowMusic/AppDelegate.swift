//
//  AppDelegate.swift
//  ShowMusic
//
//  Created by Dave Henke on 8/10/18.
//  Copyright © 2018 ShowPad, NV. All rights reserved.
//

import UIKit
import ShowMusicServices

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: PlaylistsViewController(nibName: "PlaylistsViewController", bundle: nil))
        window?.makeKeyAndVisible()
        return true
    }
}

