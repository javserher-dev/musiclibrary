//
//  Constants.swift
//  ShowMusic
//
//  Created by Javier Servate on 11/04/2019.
//  Copyright © 2019 ShowPad, NV. All rights reserved.
//

import UIKit

struct Constants {
    static let placeholderCover = "placeholderCover"
    
    struct CellIdentifiers {
        static let playlistTableView =  "playlistTableViewCell"
        static let albumCollectionView = "albumCollectionViewCell"
    }
    
    struct NibNames {
        static let playListTableViewCell = "PlaylistTableViewCell"
    }
    
    struct Labels {
        static let libraryNavigationBar = "My Library"
    }
}
