//
//  CacheManager.swift
//  ShowMusic
//
//  Created by Javier Servate on 11/04/2019.
//  Copyright © 2019 ShowPad, NV. All rights reserved.
//

import UIKit

class CacheManager: NSObject {
    static let shared = CacheManager()
    let imageCache = NSCache<NSString, UIImage>()
    
    func saveImage(image: UIImage, for key: NSString) {
        imageCache.setObject(image, forKey: key)
    }
    
    func retrieveImage(for key: NSString) -> UIImage? {
        return imageCache.object(forKey: key)
    }
}
